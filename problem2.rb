# frozen_string_literal: true

class Array
  def to_histogram
    histogram = Hash.new { 0 }
    each { |value| histogram[value] += 1 }
    histogram
  end
end
