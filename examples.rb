# frozen_string_literal: false

require_relative 'problem1'
require_relative 'problem2'
require_relative 'problem3'
require_relative 'problem4'
require_relative 'problem5'

puts 'Problem 1:'

my_string = ''
string_iterator('a', 'me') { |value| my_string += "#{value}, " }
my_string[-2..-1] = '.'
puts my_string
puts

puts 'Problem 2:'

my_histogram = [1, 2, 3, 2, 3, 2].to_histogram
puts my_histogram
puts

puts 'Problem 3:'

my_array = [1, 2, 3, 4, 7, 5, 5, 7]

puts mean(my_array)
puts median(my_array)
print mode(my_array)
puts
puts

puts 'Problem 4:'

paragraph = clear_paragraph('strings.txt')
puts paragraph

print match_start_of_word(paragraph, 'dis')
puts
print match_end_of_word(paragraph, 'ing')
puts
puts

puts 'Problem 5:'

DATE_FORMATS = ['%d/%m/%Y %H:%M:%S', '%d/%m/%Y %l:%M:%S %p'].freeze
dates = parse_dates('events.csv', DATE_FORMATS, Rational(-5, 24))
valid_dates = dates[0]
invalid_dates = dates[1]
generate_csv('valid_dates.csv', valid_dates)
generate_csv('invalid_dates.csv', invalid_dates)
graph = bar_graph('valid_dates', valid_dates.length, 'invalid_dates', invalid_dates.length)
append_to_file('valid_dates.csv', graph)
