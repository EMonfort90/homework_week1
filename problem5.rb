# frozen_string_literal: true

require 'csv'

def parse_dates(route, date_formats, offset = Rational(0, 24))
  valid_dates = []
  invalid_dates = []
  line = 0
  CSV.foreach(route) do |row|
    line += 1
    event = row[0]
    date = row[1]
    date_formats.each do |format|
      date = DateTime.strptime(date, format)
      date = date.new_offset(offset)
      valid_dates << [event, date]
      break
    rescue Date::Error
      puts("#{line}: #{date}")
      invalid_dates << [line, event, date.to_s]
      break
    end
  end
  [valid_dates, invalid_dates]
end

def generate_csv(name, data)
  CSV.open(name, 'w') do |csv|
    data.each { |event| csv << event }
  end
end

def bar_graph(item_one, value_item_one, item_two, value_item_two)
  graph = ''
  graph_length = [value_item_one, value_item_two].max
  max_digits = (graph_length - 1).to_s.length
  graph_length.downto(1) do |i|
    item_one_value = value_item_one < i ? ' ' : 'o'
    item_two_value = value_item_two < i ? ' ' : 'x'
    extra_spaces = max_digits - i.to_s.length
    graph += "#{i}#{' ' * extra_spaces}|   #{item_one_value}      #{item_two_value}\n"
  end
  graph += "#{'-' * 20}\n"
  graph += "References:\n"
  graph += "#{item_one}: 'o'\n"
  graph += "#{item_two}: 'x'\n"
end

def append_to_file(route, string)
  File.open(route, 'a') do |file|
    file.puts string
  end
end
