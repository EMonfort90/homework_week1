# frozen_string_literal: true

require_relative 'problem2'

def mean(values)
  sum = 0.0
  values.each { |value| sum += value }
  sum / values.length
end

def median(values)
  values.sort!
  length = values.length
  length.even? ? mean([values[length / 2], values[length / 2 - 1]]) : values[length / 2]
end

def mode(values)
  histogram = values.to_histogram
  max_repetitions = 0
  histogram.each do |_value, repetitions|
    max_repetitions = repetitions if repetitions > max_repetitions 
  end
  histogram.select! { |_value, repetitions| repetitions == max_repetitions }
  histogram.keys
end
