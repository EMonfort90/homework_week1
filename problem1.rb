# frozen_string_literal: false

def string_iterator(start, finish, &action)
  (start..finish).each { |value| action.call(value) }
end
