# frozen_string_literal: false

def clear_paragraph(route)
  paragraph = File.read(route)

  # Eliminating spaces between words
  paragraph = paragraph.gsub(/(\w)[ \t]+(\w)/, '\1 \2')

  # Eliminating spaces word and punctuation
  paragraph = paragraph.gsub(/(\w)\s+([.,:;?!])/, '\1\2')

  # Eliminating spaces after punctuation sign of end of string'
  paragraph = paragraph.gsub(/([.,:;!?])\s+\Z/, '\1')

  # Eliminating spaces after punctuation and break line'
  paragraph = paragraph.gsub(/([.,:;!?])\s+\R/, "\\1\n")

  # Eliminating break lines between words'
  paragraph = paragraph.gsub(/([\w,:;!?])\R+/, '\1 ')

  # Eliminating extra spaces between dot and word'
  paragraph = paragraph.gsub(/([..,:;?!]) +(\w)/, '\1 \2')

  # Eliminating spaces before at the start of a line
  paragraph = paragraph.gsub(/^\s*/, '')

  # Eliminating spaces and periods united
  paragraph.gsub(/\.+ *\.+/, '.')
end

def match_start_of_word(text, match, case_sensitive: true)
  my_regex = Regexp.new("#{match}\\w*", case_sensitive)
  text.scan(my_regex)
end

def match_end_of_word(text, match, case_sensitive: true)
  my_regex = Regexp.new("\\w*#{match}", case_sensitive)
  text.scan(my_regex)
end
